const fs = require('fs');

global.config = {};

function loadConfig(fileName = 'config.json', charset = 'utf8') {
    try {
        const configFile = fs.readFileSync(fileName, charset);
        global.config = JSON.parse(configFile);
        console.log("Config loaded successfully.");
    } catch (err) {
        console.error("Error loading config file:", err);
    }
}


function updateConfig(fileName, charset, newConfig) {
    try {
        const configFile = JSON.stringify(newConfig, null, 2); // 格式化存储，缩进为2
        fs.writeFileSync(fileName, configFile, charset);
        global.config = newConfig;
        console.log("Config file updated successfully.");
    } catch (err) {
        console.error("Error updating config file:", err);
    }
}


module.exports = {
    loadConfig,
    updateConfig
};


// // 示例用法
// const fileName = 'custom_config.json'; // 自定义文件名
// const charset = 'utf8'; // 自定义字符集
// let config = loadConfig(fileName, charset);
// console.log("Loaded config:", config);

// config.maxItemsPerPage = 20;
// updateConfig(fileName, charset, config);
